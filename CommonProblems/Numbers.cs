﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interviews
{
	public class Numbers
	{
		public static int Factorial(int n)
		{
			if (n < 0) return 0;
			if (n == 0) return 1;

			return n * (Numbers.Factorial(n - 1));
		}

		public static int FactorialNoRecursion(int n)
		{
			if (n < 0) return 0;
			if (n == 0) return 1;

			int result = 1;
			for (int i = n; i > 0; i--)
			{
				result *= i;
			}

			return result;
		}

		//https://www.geeksforgeeks.org/time-complexity-recursive-fibonacci-program/ BigO: 2^n or 1.6180^n golden ration (φ)
		public static int Fibonacci(int n)
		{
			if (n < 0) return 0;

			if (n == 0)
				return 0;
			else if (n == 1)
				return 1;
			else
				return Fibonacci(n - 1) + Fibonacci(n - 2);
		}

		public static long FibonacciOpt(int n)
		{
			if (n < 0) return 0;

			long[] cache = new long[n + 1];
			for (int i = 0; i <= n; i++)
				cache[i] = 0;

			return Numbers.Fibonacci(n, cache);
		}

		private static long Fibonacci(int n, long[] cache)
		{
			if (n == 0)
				return 0;
			else if (n == 1)
				return 1;

			if (cache[n] == 0)
				cache[n] = Fibonacci(n - 1, cache) + Fibonacci(n - 2, cache);

			return cache[n];
		}


		public static int FibonacciNoRecursion(int n)
		{
			if (n < 0) return 0;

			if (n == 0)
				return 0;
			else if (n == 1)
				return 1;

			int first = 0, second = 1, result = 1;

			for (int i = 2; i <= n; i++)
			{
				result = first + second;
				first = second;
				second = result;
			}

			return result;
		}
	}
}
