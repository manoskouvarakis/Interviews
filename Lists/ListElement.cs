﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interviews
{
	public class ListElement
	{
		public ListElement Next { get; set; }
		public int IntData { get; set; }

		public ListElement(int data)
		{
			this.IntData = data;
		}

		public static void Print(ListElement head)
		{
			while (head != null)
			{
				Console.WriteLine(head.IntData);
				head = head.Next;
			}
			Console.WriteLine("----------");
		}

		public static ListElement Find(ListElement head, int data)
		{
			while (head != null)
			{
				if (head.IntData == data) break;
				head = head.Next;
			}

			return head;
		}

		public static ListElement InsertStart(ListElement head, int data)
		{
			if (head == null) throw new Exception("Input header is null");

			ListElement newItem = new ListElement(data);
			newItem.Next = head;
			return newItem;
		}

		public static void InsertEnd(ListElement head, int data)
		{
			if (head == null) throw new Exception("Input header is null");

			while (head.Next != null)
			{
				head = head.Next;
			}

			ListElement newItem = new ListElement(data);
			head.Next = newItem;
		}

		public static bool Delete(ref ListElement head, int toDelete)
		{
			if (head == null) throw new Exception("Input header is null");

			if (head.IntData == toDelete)
			{
				head = head.Next;
				return true;
			}

			ListElement cursor = head;

			while (cursor != null)
			{
				if (cursor.Next != null && cursor.Next.IntData == toDelete)
				{
					cursor.Next = cursor.Next.Next;
					return true;
				}
				cursor = cursor.Next;
			}

			return false;
		}
	}

	public static class ListUtilities
	{
		public static ListElement MToLastElement(ListElement head, int m)
		{
			if (head == null) return null;

			ListElement cursor = head;
			if (m == 0)
			{
				while (cursor.Next != null)
				{
					cursor = cursor.Next;
					return cursor;
				}
			}

			ListElement forwardCursor = head;
			for (int i = 0; i < m; i++)
			{
				if (forwardCursor.Next != null) forwardCursor = forwardCursor.Next;
				else return null;
			}

			while (forwardCursor.Next != null)
			{
				cursor = cursor.Next;
				forwardCursor = forwardCursor.Next;
			}

			return cursor;
		}

		public static bool IsCycle(ListElement head)
		{
			if (head == null) return false;

			ListElement slow = head;
			ListElement fast = head;

			if (slow.Next != null) slow = slow.Next;
			else return false;

			if (fast.Next.Next != null) fast = fast.Next.Next;
			else return false;

			//while (fast != null && fast.Next != null)
			//{
			//	if (fast == slow || fast.Next == slow) return true;
			//	fast = fast.Next.Next;
			//	slow = slow.Next;
			//}

			while (true)
			{
				if (fast == null || fast.Next == null) return false;
				if (fast == slow || fast.Next == slow) return true;
				fast = fast.Next.Next;
				slow = slow.Next;
			}
		}
	}
}
